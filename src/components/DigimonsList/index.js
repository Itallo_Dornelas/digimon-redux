import { useSelector } from "react-redux";

const DigimonsList = () => {
  const { digimons } = useSelector((state) => state);

  return (
    <div>
      <ul>
        {digimons.map((digimon, index) => {
          return <li key={index}>{digimon.name}</li>;
        })}
      </ul>
    </div>
  );
};

export default DigimonsList;
