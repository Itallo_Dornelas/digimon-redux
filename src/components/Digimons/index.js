import { useSelector } from "react-redux";

const Digimons = () => {
  const { digimons } = useSelector((state) => state);

  return (
    <div className="digimonsContainer">
      {digimons.map((digimon, index) => {
        return (
          <div className="digimonCard" key={index}>
            <h4>{digimon.name}</h4>
            <img src={digimon.img} alt={digimon.name} />
            <h5>{digimon.level}</h5>
          </div>
        );
      })}
    </div>
  );
};

export default Digimons;
