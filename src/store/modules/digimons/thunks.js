import api from "../../../services/api";
import { addDigimon } from "./actions";
import { toast } from "react-toastify";

const addDigimonsThunk = (digimon) => {
  return (dispatch) => {
    api
      .get(digimon)
      .then((response) => dispatch(addDigimon(response.data[0])))
      .catch((_) => toast.error("Digimon não existe!!"));
  };
};

export default addDigimonsThunk;
