import { ADD_DIGIMONS } from "./actionsTypes";

export const addDigimon = (digimons) => ({
  type: ADD_DIGIMONS,
  digimons,
});
