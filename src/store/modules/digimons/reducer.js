import { ADD_DIGIMONS } from "./actionsTypes";

const digimonsReducer = (state = [], action) => {
  switch (action.type) {
    case ADD_DIGIMONS:
      const { digimons } = action;
      return [...state, digimons];

    default:
      return state;
  }
};

export default digimonsReducer;
