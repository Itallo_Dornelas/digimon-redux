import "./App.css";
import Digimons from "./components/Digimons";
import DigimonsList from "./components/DigimonsList";
import Search from "./components/Search";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
function App() {
  return (
    <div className="App-header">
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />

      <Search />
      <DigimonsList />
      <Digimons />
    </div>
  );
}

export default App;
